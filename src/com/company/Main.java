package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Main main = new Main();
        Scanner scan = new Scanner(System.in);
        float w1=0;
        float w2 =0;
        float w3=0 ;
        //处理输入小于0
        while ( (w1 = scan.nextFloat())<0 ){
            System.out.println("输入错误w1:   "+w1);
        }
        while ( (w2 = scan.nextFloat())<0 ){
            System.out.println("输入错误w2:   "+w2);
        }
        while ( (w3 = scan.nextFloat())<0 ){
            System.out.println("输入错误w3:   "+w3);
        }
//        main.one(w1,w2);
//        main.two(w1,w2,w3);
//        main.three(w1,w2,w3);
//        main.four(w1,w2,w3);
        System.out.println( "问题一： "+main.one(w1,w2));
        System.out.println( "问题二： "+main.two(w1,w2,w3) );
        System.out.println( "问题三： "+main.three(w1,w2,w3) );
        System.out.println( "问题四： "+main.four(w1,w2,w3) );

    }

 /*           1、有一家超市，出售苹果和草莓。其中苹果 8 元/斤，草莓 13 元/斤。
        现在顾客 A 在超市购买了若干斤苹果和草莓，需要计算一共多少钱？
        请编写函数，对于 A 购买的水果斤数 (水果斤数为大于等于 0 的整数)，计算并返回所购买商品的总价。*/
    public float one(float w1,float w2){
        FruitEntity apple = new FruitEntity();
        apple.setWeight(w1);
        apple.setPrice(8);
        FruitEntity strawberry = new FruitEntity();
        strawberry.setWeight(w2);
        strawberry.setPrice(13);
        float money=sum(apple.getWeight(),apple.getPrice(),0)+sum(strawberry.getWeight(),strawberry.getPrice(),0);
        return money;

    }
/*    2、超市增加了一种水果芒果，其定价为 20 元/斤。
    现在顾客 B 在超市购买了若干斤苹果、 草莓和芒果，需计算一共需要多少钱？
    请编写函数，对于 B 购买的水果斤数 (水果斤数为大于等于 0 的整数)，计算并返回所购买商品的总价。*/
    public float two(float w1,float w2,float w3){
        FruitEntity apple = new FruitEntity();
        apple.setWeight(w1);
        apple.setPrice(8);
        FruitEntity strawberry = new FruitEntity();
        strawberry.setWeight(w2);
        strawberry.setPrice(13);
        FruitEntity manguo = new FruitEntity();
        manguo.setWeight(w2);
        manguo.setPrice(20);
        float money=sum(apple.getWeight(),apple.getPrice(),0)+sum(strawberry.getWeight(),strawberry.getPrice(),0)
                +sum(manguo.getWeight(),manguo.getPrice(),0);
        return money;

    }

/*    3、超市做促销活动，草莓限时打 8 折。
    现在顾客 C 在超市购买了若干斤苹果、 草莓和芒果，需计算一共需要多少钱？
    请编写函数，对于 C 购买的水果斤数 (水果斤数为大于等于 0 的整数)，计算并返回所购买商品的总价。*/
    public float three(float w1,float w2,float w3){
        FruitEntity apple = new FruitEntity();
        apple.setWeight(w1);
        apple.setPrice(8);
        FruitEntity strawberry = new FruitEntity();
        strawberry.setWeight(w2);
        strawberry.setPrice(13);
        strawberry.setDiscount(0.8f);
        FruitEntity manguo = new FruitEntity();
        manguo.setWeight(w2);
        manguo.setPrice(20);
        float money=sum(apple.getWeight(),apple.getPrice(),0)+sum(strawberry.getWeight(),strawberry.getPrice(),strawberry.getDiscount())
                +sum(manguo.getWeight(),manguo.getPrice(),0);
        return money;

    }

//    4、促销活动效果明显，超市继续加大促销力度，购物满 100 减 10 块。
//    现在顾客 D 在超市购买了若干斤苹果、 草莓和芒果，需计算一共需要多少钱？
//    请编写函数，对于 C 购买的水果斤数 (水果斤数为大于等于 0 的整数)，计算并返回所购买商品的总价。
    public float four(float w1,float w2,float w3){
        FruitEntity apple = new FruitEntity();
        apple.setWeight(w1);
        apple.setPrice(8);
        FruitEntity strawberry = new FruitEntity();
        strawberry.setWeight(w2);
        strawberry.setPrice(13);
//        strawberry.setDiscount(0.8f);
        FruitEntity manguo = new FruitEntity();
        manguo.setWeight(w2);
        manguo.setPrice(20);
        float money=sum(apple.getWeight(),apple.getPrice(),0)+sum(strawberry.getWeight(),strawberry.getPrice(),0)
                +sum(manguo.getWeight(),manguo.getPrice(),0);
        if(money>=100){
            money=money-(money/100)*10;
        }
        return money;

    }

    //计数单个水果总价
    public float sum(float weight,float price,float discount){
        float money=0;
        if(discount!=0){
             money=weight*price*discount;
        }else {
             money=weight*price;
        }
        return money;
    }
}
